<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $table = "posts";
    protected $guarded = ['id'];

    public function get_user()
    {
        return $this->belongsTo('App\Models\User','create_by','id');
    }

    public function get_like()
    {
        return $this->hasMany('App\Models\Like','post_id','id');
    }

    public function get_comment()
    {
        return $this->hasMany('App\Models\Comment','post_id','id')->with('get_user');
    }
}
