<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;
    protected $table = "comments";
    protected $guarded = ['id'];

    public function get_user()
    {
        return $this->belongsTo('App\Models\User','parent_id','id');
    }
}
