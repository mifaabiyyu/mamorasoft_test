<?php

use App\Http\Controllers\CommentController;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\PostsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', [PostsController::class, 'index'])->name('dashboard');

    Route::post('/posts/store', [PostsController::class, 'store'])->name('posts.store');
    Route::get('/posts/show/{id}', [PostsController::class, 'show'])->name('posts.show');
    Route::post('/like/store', [LikeController::class, 'store'])->name('likes.store');
    Route::post('/comment/store', [CommentController::class, 'store'])->name('comment.store');
    // Route::post('/comment/store', );
});

require __DIR__.'/auth.php';
